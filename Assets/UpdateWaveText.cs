﻿
using UnityEngine;
using TMPro;

public class UpdateWaveText : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI waveText = null;

    private void FixedUpdate() {
        waveText.SetText("Vague : " + MapManager.GetInstance().getCurrentWaveIndex());
    }
}
