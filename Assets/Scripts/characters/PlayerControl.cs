﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public CharacterController controller;

    [SerializeField]
    private Player player = null;

    private float smoothTurnTime = 0.1f;
    private float smoothTurnVelocity;

    private float attackCD = 0.5f;
    private float specialCD = 1f;

    private float yAdjust; 
    // private bool isAttacking = false;

    private float hInput, vInput;

    void Start()
    {
        yAdjust = transform.position.y;
    }

    void FixedUpdate()
    {
        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(hInput, 0f, vInput).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float rawAngle = Mathf.Atan2(-direction.z, direction.x) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, rawAngle, ref smoothTurnVelocity, smoothTurnTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            controller.Move(player.speed * direction * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, yAdjust, transform.position.z);
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (attackCD <= 0)
            {
                player.Attack(1.5f,player.weapon,player.power);
                attackCD = 0.5f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (specialCD <= 0)
            {
                player.Attack(2f,player.playerBody,player.specialPower);
                specialCD = 1f;
            }
        }

        attackCD -= Time.deltaTime;
        specialCD -= Time.deltaTime;
    }
}