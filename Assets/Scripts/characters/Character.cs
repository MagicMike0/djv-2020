using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public int hp;
    public int maxHP;

    public int power;
    public int specialPower;
    public float speed;

    [SerializeField]
    public GameObject weapon;

    public float invincibilityPeriod = 0.4f;
    public bool isDead = false;

    protected abstract void Spawn();
    public abstract void Despawn();
    public abstract void Attack(float attackRadius, GameObject attackObject, int damage);
    
    public void Update()
    {
        invincibilityPeriod -= Time.deltaTime;
        if (isDead)
            Despawn();
    }

    public void TakeDamage(int damage)
    {
        if (invincibilityPeriod <= 0)
        {
            hp -= damage;
            if(hp <= 0) {
                isDead = true;
            }
            invincibilityPeriod = 0.4f;
        }
    }

    public void Heal(int heal)
    {
        hp += heal;
        if(hp > maxHP) {
            hp = maxHP;
        }
    }
}
