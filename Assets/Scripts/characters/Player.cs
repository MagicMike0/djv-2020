using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Character {
    [SerializeField]
    public GameObject playerBody;

    [SerializeField]
    private Canvas loseCanvas = null;

    protected void Start()
    { 
        hp = 100;
        maxHP = 100;
        power = 35;
        specialPower = 100;
        speed = 10;
        Spawn();
    }

    protected override void Spawn()
    {
        this.gameObject.SetActive(true);
    }

    public override void Despawn()
    {
        //Animator.SetBool(isActiveAndEnabled, true);
        loseCanvas.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public override void Attack(float attackRadius, GameObject attackObject, int damage)
    {
        Collider[] hitColliders = Physics.OverlapSphere(attackObject.transform.position, attackRadius);
        Character hitCharacter;
        foreach (Collider hitCollider in hitColliders)
        {
            hitCharacter = hitCollider.GetComponent<Character>();
            if(hitCharacter != null)
            {
                if (hitCharacter.gameObject.CompareTag("Foe"))
                {
                    hitCharacter.TakeDamage(damage);
                }
            }
        }
    }
}
