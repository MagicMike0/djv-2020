﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementRigi : MonoBehaviour
{
    [SerializeField]
    private Player player = null;

    [SerializeField]
    private CharacterController controller;

    private float attackCD = 0.5f;
    private float specialCD = 2f;

    [SerializeField]
    private Rigidbody rigi = null;
    
    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    private float maxVelocity = 5;
    [SerializeField]
    private float acceleration = 5;
    [SerializeField]
    private float decceleration = 10;

    public Transform targetDirection;
    private Vector3 verticalMovement = Vector3.zero;
    private Vector3 horizontalMovement = Vector3.zero;
    private Vector3 movementVector = Vector3.zero;
    private Vector3 motionVector = Vector3.zero;
    private Vector3 direction;

    public bool isDead = false;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.Z))
            verticalMovement = Vector3.forward * Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.S))
            verticalMovement = -Vector3.forward * Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.D))
            horizontalMovement = Vector3.right * Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.Q))
            horizontalMovement = -Vector3.right * Input.GetAxisRaw("Horizontal");

        movementVector = (verticalMovement + horizontalMovement).normalized;

        UpdateAnimator();

        if (player.isDead)
            player.Despawn();

        DoRotation();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (attackCD <= 0)
            {
                player.Attack(0.5f,player.weapon,player.power);
                attackCD = 0.5f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (specialCD <= 0)
            {
                player.Attack(2f,player.playerBody,player.specialPower);
                specialCD = 2f;
            }
        }

        attackCD -= Time.deltaTime;
        specialCD -= Time.deltaTime;
    }


    private void FixedUpdate()
    {
        motionVector = new Vector3(movementVector.x * maxVelocity, rigi.velocity.y, movementVector.z * maxVelocity);
        float lerpSmooth = rigi.velocity.magnitude < motionVector.magnitude ? acceleration : decceleration;
        rigi.velocity = Vector3.Lerp(rigi.velocity, motionVector, lerpSmooth / 20);

        if (direction.magnitude > 0)
            transform.forward = Vector3.Lerp(transform.forward, direction, .3f);
    }

    private void DoRotation()
    {
        var camera = Camera.main;
        var mousePosition = Input.mousePosition;
        mousePosition.z = 1;

        Vector3 targetPosition = Vector3.zero;
        RaycastHit hit;

        if (Physics.Raycast(camera.ScreenPointToRay(mousePosition), out hit, 1000))
        {
            targetPosition = hit.point;
        }
        
        if (targetDirection && targetPosition != Vector3.zero)
        {
            targetPosition.y = transform.position.y;
            direction = targetPosition - transform.position;
            direction.y = 0;
            direction = direction.normalized;

            targetPosition.y = targetDirection.position.y;
            targetDirection.position = targetPosition;

            targetDirection.rotation = Quaternion.identity;
        }
    }

    private void UpdateAnimator()
    {
        animator.SetFloat("velocityForward", Vector3.Dot(rigi.velocity, transform.forward));
        animator.SetFloat("velocityRight", Vector3.Dot(rigi.velocity, transform.right));
        animator.SetFloat("velocity", rigi.velocity.magnitude);
        animator.SetBool("isDead", isDead);
    }
}