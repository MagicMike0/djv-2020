﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorEnnemy : Character
{
    [SerializeField]
    private WaveController wave = null;

    protected override void Spawn()
    {
        this.gameObject.SetActive(true);
    }

    public override void Despawn()
    {
        if (wave != null)
            wave.FoeKilled(this.gameObject);
        else
            Destroy(this.gameObject);
    }

    public override void Attack(float attackRadius, GameObject attackObject, int damage)
    {
        Collider[] hitColliders = Physics.OverlapSphere(attackObject.transform.position, attackRadius);
        foreach (Collider hitCollider in hitColliders)
        {
            Character hitCharacter = hitCollider.GetComponent<Character>();
            if(hitCharacter != null) {
                if (hitCharacter.gameObject.CompareTag("Player")) {
                    hitCharacter.TakeDamage(damage);
                }
            }
        }
    }
}
