﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyDistController : MonoBehaviour
{
    [SerializeField] private float maxX = 0;

    [SerializeField] private float minX = 0;

    [SerializeField] private float maxZ = 0;

    [SerializeField] private float minZ = 0;

    [SerializeField] private GameObject cible = null;

    [SerializeField] private float tooClose = 5;

    [SerializeField] private Animator animator = null;

    [SerializeField] private float speedrotation = 2;

    [SerializeField] private Arrow arrowOriginal = null;

    private float distanceplayer;

    private Vector3 destination;

    public NavMeshAgent agent;

    private bool move;

    private float time;

    private bool canHit;

    private float centerx;

    private float centerz;

    private float speed;

    private float rotation;

    private float x;

    private float z;

    private Quaternion from;

    private Quaternion to;

    private bool rotate;

    private bool hasAttack;

    private float nbArrow;

    // Start is called before the first frame update
    void Start()
    {
        arrowOriginal.gameObject.SetActive(false);
        UpdateAnimator();
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 4 && !hasAttack)
        {
            nbArrow = Random.Range(1, 6);
            StartCoroutine(Attack());
        }
        time += Time.deltaTime;
        distanceplayer = Vector3.Distance(transform.position, cible.transform.position);
        if (distanceplayer < tooClose)
        {
            move = true;
        }
        if (agent.remainingDistance < agent.stoppingDistance)
        {
            agent.speed = 0;
            UpdateAnimator();
            rotate = true;
        }
        if (rotate)
        {
            Rotate();
        }
        destination = ClosestCorner(cible.transform.position);
    }

    private void FixedUpdate()
    {
        if (move)
        {
            agent.speed = 2;
            UpdateAnimator();
            rotate = false;
            move = false;
            agent.SetDestination(destination);
        }
    }

    private void UpdateAnimator()
    {
        animator.SetBool("CanHit", canHit);
        animator.SetFloat("SpeedX", agent.speed);
    }

    private IEnumerator Attack()
    {
        hasAttack = true;
        rotate = true;
        speed = agent.speed;
        agent.speed = 0;
        canHit = true;
        animator.Rebind();
        yield return new WaitForSeconds(1f);
        Vector3 arrowPos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
        for(int i = 0; i < nbArrow; i++)
        {
            var newArrow = Instantiate(arrowOriginal, arrowPos, transform.rotation);
            newArrow.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.7f - (nbArrow * 0.1f));
        canHit = false;
        agent.speed = speed;
        UpdateAnimator();
        rotate = false;
        hasAttack = false;
        time = 0;
    }

    private void Rotate()
    {
        x = cible.transform.position.x - transform.position.x;
        z = cible.transform.position.z - transform.position.z;
        rotation = Mathf.Atan(x / z) * (180 / Mathf.PI);
        if (z < 0 && x >= 0)
        {
            rotation -= 180;
        }
        if (x < 0 && z < 0)
        {
            rotation += 180;
        }
        from = agent.gameObject.transform.rotation;
        to = Quaternion.Euler(0, rotation, 0);
        agent.gameObject.transform.rotation = Quaternion.Lerp(from, to, Time.deltaTime * speedrotation);
    }

    private Vector3 ClosestCorner(Vector3 pos)
    {
        centerx = (minX + maxX) / 2;
        centerz = (minZ + maxZ) / 2;
        if (pos == new Vector3(centerx, pos.y, centerz))
        {
            return new Vector3(maxX, 0, maxZ);
        }
        else
        {
            if (pos.x > centerx)
            {
                if (pos.z > centerz)
                {
                    return new Vector3(minX, 0.5f, minZ);
                }
                else
                {
                    return new Vector3(minX, 0.5f, maxZ);
                }
            }
            else
            {
                if (pos.z > centerz)
                {
                    return new Vector3(maxX, 0.5f, minZ);
                }
                else
                {
                    return new Vector3(maxX, 0.5f, maxZ);
                }
            }
        }
    }
}

