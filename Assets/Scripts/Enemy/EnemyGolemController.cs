﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyGolemController : MonoBehaviour
{
    [SerializeField] private float maxX = 0;

    [SerializeField] private float minX = 0;

    [SerializeField] private float maxZ = 0;

    [SerializeField] private float minZ = 0;

    [SerializeField] private GameObject cible = null;

    [SerializeField] private float tooClose = 5;

    [SerializeField] private Animator animator = null;

    [SerializeField] private Rock rockOriginal;

    private float distanceplayer;

    private Vector3 destination;

    public NavMeshAgent agent;

    private bool move;

    private float time;

    private bool canHit = false;

    private float centerx;

    private float centerz;

    private float speed;

    private bool hasAttack;

    // Start is called before the first frame update
    void Start()
    {
        UpdateAnimator();
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 10 && !hasAttack)
        {
            StartCoroutine(Attack());
        }
        time += Time.deltaTime;
        distanceplayer = Vector3.Distance(transform.position, cible.transform.position);
        if (distanceplayer < tooClose)
        {
            move = true;
        }
        if (agent.remainingDistance < agent.stoppingDistance)
        {
            agent.speed = 0;
            UpdateAnimator();
        }
        destination = OppositCorner(cible.transform.position);
    }

    private void FixedUpdate()
    {
        if (move)
        {
            agent.speed = 0.5f;
            UpdateAnimator();
            move = false;
            agent.SetDestination(destination);
        }
    }

    private void UpdateAnimator()
    {
        animator.SetBool("CanHit", canHit);
        animator.SetFloat("SpeedX", agent.speed);
    }

    private IEnumerator Attack()
    {
        hasAttack = true;
        speed = agent.speed;
        agent.speed = 0;
        canHit = true;
        UpdateAnimator();
        yield return new WaitForSeconds(5.334f);
        canHit = false;
        agent.speed = speed;
        hasAttack = false;
        UpdateAnimator();
        time = 0;
        for(int i = 0; i < 5; i++)
        {
            var newRock = Instantiate(rockOriginal, transform.position, transform.rotation);
            newRock.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
        }
    }

    private Vector3 OppositCorner(Vector3 pos)
    {
        centerx = (minX + maxX) / 2;
        centerz = (minZ + maxZ) / 2;
        if (pos == new Vector3(centerx, pos.y, centerz))
        {
            return new Vector3(maxX, 0, maxZ);
        }
        else
        {
            if (pos.x > centerx)
            {
                if (pos.z > centerz)
                {
                    return new Vector3(maxX, 0.5f, minZ);
                }
                else
                {
                    return new Vector3(maxX, 0.5f, maxZ);
                }
            }
            else
            {
                if (pos.z > centerz)
                {
                    return new Vector3(minX, 0.5f, minZ);
                }
                else
                {
                    return new Vector3(minX, 0.5f, maxZ);
                }
            }
        }
    }
}