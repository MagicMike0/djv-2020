﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    [SerializeField] private float yspawn = 5;

    [SerializeField] private float xmax = -1;

    [SerializeField] private float xmin = -29;

    [SerializeField] private float zmax = 29;

    [SerializeField] private float zmin = -29;

    [SerializeField]
    private int collisionDmg = 20;

    private float x;

    private float z;

    private void OnEnable()
    {
        x = Random.Range(xmin, xmax);
        z = Random.Range(zmin, zmax);
        transform.position = new Vector3(x, yspawn, z);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Foe")) return;

        if(other.gameObject.CompareTag("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            if(player != null)
                player.TakeDamage(collisionDmg);
        }
        Destroy(gameObject);
    }
}
