﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCacController : MonoBehaviour
{
    [SerializeField] private GameObject cible = null;

    [SerializeField] private Animator animator = null;

    [SerializeField] private float stoppingDistance = 2;

    [SerializeField] private float speedrotation = 2;

    [SerializeField] private WarriorEnnemy enemy = null;

    private float distance;

    private bool atRange;

    private bool canHit;

    public NavMeshAgent agent;

    private float rotation;

    private float x;

    private float z;

    private Quaternion from;

    private Quaternion to;

    private float speed;

    private float time;

    private bool hasAttack;

    // Start is called before the first frame update
    void Start()
    {
        speed = agent.speed;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(transform.position, cible.transform.position);
        if (agent.remainingDistance < stoppingDistance)
        {
            agent.speed = 0;
            atRange = true;
            UpdateAnimator();
        }
        else
        {
            time = 0;
            atRange = false;
        }
        if (atRange)
        {
            time += Time.deltaTime;
            Rotate();
        }
        if (time > 1 && !hasAttack)
        {
            hasAttack = true;
            StartCoroutine(Attack());
        }
    }

    private void FixedUpdate()
    {
        if (distance > stoppingDistance)
        {
            agent.speed = speed;
            UpdateAnimator();
            agent.stoppingDistance = stoppingDistance;
            agent.SetDestination(cible.transform.position);
        }
    }

    private void UpdateAnimator()
    {
        animator.SetBool("CanHit", canHit);
        animator.SetFloat("SpeedX", agent.speed);
    }

    private IEnumerator Attack()
    {
        Rotate();
        canHit = true;
        animator.Rebind();
        yield return new WaitForSeconds(1f);
        enemy.Attack(2.5f, enemy.weapon, enemy.power);
        yield return new WaitForSeconds(0.833f);
        canHit = false;
        UpdateAnimator();
        hasAttack = false;
        time = 0;
    }

    private void Rotate()
    {
        x = cible.transform.position.x - transform.position.x;
        z = cible.transform.position.z - transform.position.z;
        rotation = Mathf.Atan(x / z) * (180 / Mathf.PI);
        if (z < 0 && x >= 0)
        {
            rotation -= 180;
        }
        if (x < 0 && z < 0)
        {
            rotation += 180;
        }
        from = agent.gameObject.transform.rotation;
        to = Quaternion.Euler(0, rotation, 0);
        agent.gameObject.transform.rotation = Quaternion.Lerp(from, to, Time.deltaTime * speedrotation);
    }
}
