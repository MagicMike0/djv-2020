﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float speed = 10;

    [SerializeField]
    private int collisionDmg = 10;

    private void OnEnable()
    {
        transform.position += new Vector3(0, 0.5f, 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += speed * transform.forward * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Foe")) return;

        if(other.gameObject.CompareTag("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            if(player != null)
                player.TakeDamage(collisionDmg);
        }
        Destroy(gameObject);
    }
}
