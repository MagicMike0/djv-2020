﻿using UnityEngine;

public class OverlappingRoom2Entrance : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Player"))
            MapManager.GetInstance().StartFourthWave();
    }
}
