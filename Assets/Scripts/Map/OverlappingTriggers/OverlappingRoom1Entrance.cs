﻿using UnityEngine;

public class OverlappingRoom1Entrance : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Player"))
            MapManager.GetInstance().StartFirstWave();
    }
}
