﻿using UnityEngine;

public class OverlappingHallEntrance : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Player"))
            MapManager.GetInstance().EnterArena();
    }
}