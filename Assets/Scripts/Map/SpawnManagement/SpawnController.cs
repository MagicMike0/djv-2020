﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {
    [SerializeField]
    private List<WaveController> waves = new List<WaveController>();

    private WaveController currentWave = null;
    private int currentWaveIndex = 0;

    public int GetCurrentWaveFoeCount() {
        return currentWave != null ? currentWave.GetFoesCount() : 0;
    }

    public int GetCurrentWaveIndex() {
        return currentWaveIndex;
    }

    public void EnableWave(int index) {
        if (index > 0 && index < waves.Count + 1) {
            waves[index - 1].EnableWave();
            currentWave = waves[index - 1];
            currentWaveIndex = index;
        }
    }
}
