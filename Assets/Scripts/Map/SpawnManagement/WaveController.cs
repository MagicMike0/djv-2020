﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public abstract class WaveController : MonoBehaviour {

    [SerializeField]
    protected List<GameObject> foes = new List<GameObject>();

    [SerializeField]
    protected List<Transform> spawnTransforms = new List<Transform>();

    [SerializeField]
    protected float spawnRate = 2f, spawnDelay = 1f;

    protected Queue<GameObject> spawnQueue;

    void Awake() {
        foreach(var foe in foes) foe.SetActive(false);
    }

    public int GetFoesCount() {
        return foes.Count;
    }

    public void EnableWave() {
        spawnQueue = new Queue<GameObject>(foes);
        StartCoroutine(SpawnFoe());
    }

    public void FoeKilled(GameObject foe) {
        foes.Remove(foe);
        Destroy(foe);
        if (foes.Count == 0)
            TriggerMapManager();
    }

    protected abstract void TriggerMapManager();

    protected IEnumerator SpawnFoe() {
        yield return new WaitForSeconds(spawnDelay);
        while (spawnQueue.Count > 0) {
            GameObject currentFoe = spawnQueue.Dequeue();

            // set custom spawn position
            if (spawnTransforms.Count > 0) {
                int spawnIndex = Random.Range(0, spawnTransforms.Count);
                Vector3 pos = new Vector3(spawnTransforms[spawnIndex].position.x, currentFoe.transform.position.y, spawnTransforms[spawnIndex].position.z);
                currentFoe.transform.position = pos;
                // set custom spawn rotation
                currentFoe.transform.rotation = spawnTransforms[spawnIndex].rotation;
            }

            // spawn the foe
            currentFoe.SetActive(true);
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
