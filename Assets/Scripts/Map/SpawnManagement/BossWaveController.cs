﻿public class BossWaveController : WaveController {
    protected override void TriggerMapManager() {
        MapManager.GetInstance().EndBossWave();
    }
}
