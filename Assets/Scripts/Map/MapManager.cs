﻿using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour {
    
    // triggers linked to arena colliders
    private bool enterArenaTrigger = false
    , firstRoomTrigger = false, endFirstRoomTrigger = false
    , secondRoomTrigger = false, endSecondRoomTrigger = false
    , thirdRoomTrigger = false;

    [SerializeField]
    private Canvas winCanvas = null;

    [SerializeField]
    private MapStateController mapController = null;

    [SerializeField]
    private SpawnController spawnController = null;

    private static MapManager instance;

    public static MapManager GetInstance() {
        return instance;
    }

    public void Awake() {
        if (instance != null) {
            Debug.LogError("Instance of MapManager already  exists.");
            Destroy(this);
        }
        else
            instance = this;
    }

    public int getCurrentWaveFoeCount() {
        return spawnController.GetCurrentWaveFoeCount();
    }

    public int getCurrentWaveIndex() {
        return spawnController.GetCurrentWaveIndex();
    }

    public void EnterArena() {
        if (enterArenaTrigger) return;
        mapController.OpenHall1ToRoom1();
        enterArenaTrigger = true;
    }

    public void StartFirstWave() {
        if (firstRoomTrigger) return;
        mapController.CloseRoom1ToHall1();
        spawnController.EnableWave(1);
        firstRoomTrigger = true;
    }

    public void StartSecondWave() {
        spawnController.EnableWave(2);
    }

    public void StartThirdWave() {
        spawnController.EnableWave(3);
    }

    public void EndFirstRoom() {
        if (endFirstRoomTrigger) return;
        mapController.OpenRoom1ToRoom2();
        endFirstRoomTrigger = true;
    }

    public void StartFourthWave() {
        if (secondRoomTrigger) return;
        mapController.CloseRoom2ToRoom1();
        spawnController.EnableWave(4);
        secondRoomTrigger = true;
    }

    public void StartFifthWave() {
        spawnController.EnableWave(5);
    }

    public void StartSixthWave() {
        spawnController.EnableWave(6);
    }

    public void EndSecondRoom() {
        if (endSecondRoomTrigger) return;
        mapController.OpenRoom2ToRoom3();
        endSecondRoomTrigger = true;
    }

    public void StartSeventhWave() {
        if (thirdRoomTrigger) return;
        mapController.CloseRoom3ToRoom2();
        spawnController.EnableWave(7);
        thirdRoomTrigger = true;
    }

    public void StartHeigthWave() {
        spawnController.EnableWave(8);
    }

    public void StartNinthWave() {
        spawnController.EnableWave(9);
    }

    public void EndThirdRoom() {
        mapController.OpenArena();
        spawnController.EnableWave(10);
    }

    public void EndBossWave() {
        winCanvas.gameObject.SetActive(true);
    }
}
