﻿using UnityEngine;

public class OpenableGate : MonoBehaviour {

    public Transform gate;

    [SerializeField]
    private bool open = false;
    private bool updatable = false;

    private void Awake() {
        if (open)
            gate.gameObject.SetActive(false);
        else
            gate.gameObject.SetActive(true);
    }

    public bool IsOpen() {
        return open;
    }

    public void SetGateState(bool open) {
        this.open = open;
        this.updatable = true;
    }

    private void FixedUpdate() {
        if (updatable && gate != null) {
            if (open)
                gate.gameObject.SetActive(false);
            else
                gate.gameObject.SetActive(true);
        }
        updatable = false;
    }

}
