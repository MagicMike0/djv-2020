﻿using UnityEngine;

public class MapStateController : MonoBehaviour {

    [SerializeField]
    private MapGenerator Arena = null, Hall1 = null, Room1 = null, Hall2 = null, Room2 = null, Room3 = null, BossRoom = null;

    /** Controller calls follow game's time line */

    // start
    public void OpenHall1ToRoom1() {
        OpenDoors(Hall1);
        OpenDoors(Arena);
    }

    // before first room
    public void CloseRoom1ToHall1() {
        CloseDoors(Hall1);
        CloseDoors(Arena);
    }

    // after first room
    public void OpenRoom1ToRoom2() {
        OpenDoors(Room1);
        OpenDoors(Hall2);
        OpenDoors(Room2);
    }

    // before second room
    public void CloseRoom2ToRoom1() {
        CloseDoors(Room1);
        CloseDoors(Hall2);
        CloseDoors(Room2);
    }

    // after second room
    public void OpenRoom2ToRoom3() {
        OpenDoors(Room2);
        OpenDoors(Room3);
    }

    // before third room
    public void CloseRoom3ToRoom2() {
        CloseDoors(Room2);
        CloseDoors(Room3);
    }

    // before boss room
    public void OpenArena() {
        Room1.DestroyWalls();
        Hall2.DestroyWalls();
        Room2.DestroyWalls();
        Room3.DestroyWalls();
        BossRoom.DestroyWalls();
        BossRoom.DestroyRoof();
    }

    private void CloseDoors(MapGenerator targetMap) {
        targetMap.ChangeGatesState(false, false, false, false);
    }

    private void OpenDoors(MapGenerator targetMap) {
        targetMap.ChangeGatesState(true, true, true, true);
    }
}
