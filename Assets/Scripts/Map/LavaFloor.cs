﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaFloor : MonoBehaviour {

    [SerializeField]
    private Collider lavaCollider;

    [SerializeField]
    private int collisionDmg = 20;

    private void OnTriggerStay(Collider other) {
        if(other.gameObject.CompareTag("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            if(player != null)
                player.TakeDamage(collisionDmg);
        }
        if(other.gameObject.CompareTag("Foe")) {
            WarriorEnnemy foe = other.gameObject.GetComponent<WarriorEnnemy>();
            foe.TakeDamage(collisionDmg * 10);
        }
    }
}
