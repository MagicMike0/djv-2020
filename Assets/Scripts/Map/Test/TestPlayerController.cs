﻿using UnityEngine;

public class TestPlayerController : MonoBehaviour {

    public float speed = 4f;

    void Update() {
        if (Input.anyKey)
            Move();
    }

    private void Move() {
        float hInput = Input.GetAxis("HorizontalKey");
        float vInput = Input.GetAxis("VerticalKey");
        Vector3 direction = Vector3.Normalize(new Vector3(hInput, 0, vInput));
        transform.position += speed * direction * Time.deltaTime;
        if (direction != Vector3.zero)
            transform.forward = -direction;
    }
}
