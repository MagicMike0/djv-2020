﻿using UnityEngine;

public class TestFoeController : MonoBehaviour {
    private Collider m_collider;

    [SerializeField]
    private WaveController wave = null;
    
    private void Awake() {
        m_collider = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider collision) {
        if(collision.gameObject.CompareTag("Player")) {
            // hit on collision example
            /*
            PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
            if(playerController != null)
            playerController.TakeHit(collisionDmg);
            */

            // die on collision for the wave example
            Despawn();
        }
    }

    private void Despawn() {
        // StartCoroutine(DeathEffect());
        if (wave != null)
            wave.FoeKilled(this.gameObject);
        else
            Destroy(this.gameObject);
    }
}
