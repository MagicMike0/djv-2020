﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private Transform target = null;
    private Vector3 offset;


    private void Start() {
        offset = target.position - this.transform.position;
    }

    private void Update() {
        if (target)
            transform.position = target.position - offset;
                    // transform.position = Vector3.Lerp(transform.position, target.position, 1f);

    }
}
