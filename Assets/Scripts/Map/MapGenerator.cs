﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MapGenerator : MonoBehaviour {
    [SerializeField]
    private string mapHolderName = "Generated Map";
    [SerializeField]
    private Vector2Int mapSize = new Vector2Int(10, 10);
    [SerializeField]
    private Transform tilePrefab = null, lavaTilePrefab = null, obstacleTilePrefab = null, barrelTilePrefab = null, wallTilePrefab = null, lightedWallTilePrefab = null;
    [SerializeField]
    private OpenableGate doorPrefab = null;
    [SerializeField]
    private List<Vector2Int> lavaPositions = new List<Vector2Int>(), obstaclePositions = new List<Vector2Int>(), barrelPositions = new List<Vector2Int>();
    [SerializeField]
    private int ligthFrequency = 6;
    [SerializeField]
    private int roofHeight = 5;
    [SerializeField]
    private int southGatePosition = 0, northGatePosition = 0, eastGatePosition = 0, westGatePosition = 0;
    [SerializeField]
    private bool isSouthGateOpen = false, isNorthGateOpen = false, isEastGateOpen = false, isWestGateOpen = false;
    [SerializeField]
    private bool isSouthWall = true, isNorthWall = true, isEastWall = true, isWestWall = true;
    [SerializeField]
    private bool floor = true, roof = false;
    private Transform mapHolder;
    private Transform[][] tileMap;
    private Transform[] roofTileMap;
    private OpenableGate southGate = null, northGate= null, eastGate = null, westGate = null;

    private void Start() {
        GenerateMap();
    }

    /** Map interactions */

    public void GenerateMap() {
        if (ligthFrequency < 1)
            ligthFrequency = 2;
        if (mapSize.x < 7 || mapSize.y < 7)
            mapSize = new Vector2Int(7, 7);

        InitTileMap();
        ResetMap();

        if (floor) {
            GenerateFloor();
            GenerateLava();
            GenerateObstacles();
            GenerateBarrels();
        }
        if (roof) {
            GenerateRoof();
        }

        GenerateWalls();
        GenerateGates();
    }

    public void ChangeGatesState(bool isSouthGateOpen, bool isNorthGateOpen, bool isEastGateOpen, bool isWestGateOpen) {
        if (southGate) southGate.SetGateState(isSouthGateOpen);
        if (northGate) northGate.SetGateState(isNorthGateOpen);
        if (eastGate) eastGate.SetGateState(isEastGateOpen);
        if (westGate) westGate.SetGateState(isWestGateOpen);
    }

    public void DestroyWalls() {
        for (int x = 0; x < mapSize.x; x++) {
            if (isNorthWall) {
                Transform tile = GenerateTile(tilePrefab, x, 0, 0, 0);
                IgReplaceInTileMap(x, 0, tile);
            }
            if (isSouthWall) {
                Transform tile = GenerateTile(tilePrefab, x, mapSize.y - 1, 0, 0);
                IgReplaceInTileMap(x, mapSize.y - 1, tile);
            }
        }
        
        for (int y = 1; y < mapSize.y - 1; y++) {
            if (isWestWall) {
                Transform tile = GenerateTile(tilePrefab, 0, y, 0, 0);
                IgReplaceInTileMap(0, y, tile);
            }
            if (isEastWall) {
                Transform tile = GenerateTile(tilePrefab, mapSize.x - 1, y, 0, 0);
                IgReplaceInTileMap(mapSize.x - 1, y, tile);
            }
        }
    }

    public void DestroyRoof() {
        if (roofTileMap.Length > 0) {
            for (int x = 0; x < mapSize.x; x++)
                for (int y = 0; y < mapSize.y; y++) {
                    Destroy(roofTileMap[x * mapSize.y + y].gameObject);
                }
        }
    }

    /** Getters && Setters */

    public Vector2 GetMapSize() {
        return mapSize;
    }

    public Transform[][] GetTileMap() {
        return tileMap;
    }

    /** Reset the map in the game object tree */
    private void ResetMap() {
        if (transform.Find(mapHolderName))
            DestroyImmediate(transform.Find(mapHolderName).gameObject);
        
        mapHolder = new GameObject(mapHolderName).transform;
        mapHolder.parent = transform;
    }

    /** Tiles generation */

    private void GenerateFloor() {
        for (int x = 0; x < mapSize.x; x++) {
            for (int y = 0; y < mapSize.y; y++) {
                Transform tile = GenerateTile(tilePrefab, x, y, 0, 0);
                saveInTileMap(x, y, tile);
            }
        }
    }

    private void GenerateRoof() {
        roofTileMap = new Transform[mapSize.x * mapSize.y];
        for (int x = 0; x < mapSize.x; x++)
            for (int y = 0; y < mapSize.y; y++) {
                Transform roofTile = GenerateTile(tilePrefab, x, y, roofHeight, 0);
                roofTileMap[x * mapSize.y + y] = roofTile;
            }
    }

    private void GenerateWalls() {
        Transform prefab;
        Transform tile;

        for (int x = 0; x < mapSize.x; x++) {
            prefab = 
                x < mapSize.x 
                && x > 0 
                && x % ligthFrequency != 0 ? wallTilePrefab : lightedWallTilePrefab;

            if (isNorthWall) {
                tile = GenerateTile(prefab, x, 0, 0, 0);
                replaceInTileMap(x, 0, tile);
            }
            if (isSouthWall) {
                tile = GenerateTile(prefab, x, mapSize.y - 1, 0, 180);
                replaceInTileMap(x, mapSize.y - 1, tile);  
            }
        }
        
        
        for (int y = 1; y < mapSize.y - 1; y++) {
            prefab = y % ligthFrequency != 0 ? wallTilePrefab : lightedWallTilePrefab;

            if (isWestWall) {
                tile = GenerateTile(prefab, 0, y, 0, 90);
                saveInTileMap(0, y, tile);
            }
            if (isEastWall) {
                tile = GenerateTile(prefab, mapSize.x - 1, y, 0, -90);
                saveInTileMap(mapSize.x - 1, y, tile);
            }
        }
    }

    private void GenerateGates() {
        Vector2Int center = new Vector2Int(Mathf.FloorToInt(mapSize.x / 2), Mathf.FloorToInt(mapSize.y / 2));
        if (northGatePosition > 2 && northGatePosition < mapSize.x - 3)
            northGate = GenerateGate(true, northGatePosition, 0, 0, isNorthGateOpen);
        if (southGatePosition > 2 && southGatePosition < mapSize.x - 3)
            southGate = GenerateGate(true, southGatePosition, mapSize.y - 1, 180, isSouthGateOpen);
        if (westGatePosition > 2 && westGatePosition < mapSize.y - 3)
            westGate = GenerateGate(false, 0, westGatePosition, 90, isWestGateOpen);
        if (eastGatePosition > 2 && eastGatePosition < mapSize.y - 3)
            eastGate = GenerateGate(false, mapSize.x - 1, eastGatePosition, -90, isEastGateOpen);
    }

    private OpenableGate GenerateGate(bool axeX, int x, int y, int rotation, bool state) {
        int xFactor = axeX ? 1 : 0;
        int yFactor = axeX ? 0 : 1;

        /** Instantiate the gate and set the door state */
        Vector3 pos = new Vector3(mapSize.x / 2 - x, 0, mapSize.y / 2 - y); // mapSize / 2 to center the map
        OpenableGate gate = Instantiate(doorPrefab, relativePosition(pos), Quaternion.Euler(0, rotation, 0));
        gate.transform.parent = mapHolder;
        gate.SetGateState(state);

        for (int i = 1; i < 3; i++) {
            deleteInTileMap(x - xFactor * i, y - yFactor * i);
            deleteInTileMap(x + xFactor * i, y + yFactor * i);
        }
        replaceInTileMap(x, y, gate.transform);
        return gate;
    }

    private void GenerateLava() {
        foreach (var lavaPosition in lavaPositions) {
            Transform tile = GenerateTile(lavaTilePrefab, lavaPosition.x, lavaPosition.y, 0, 0);
            replaceInTileMap(lavaPosition.x, lavaPosition.y, tile);
        }
    }

    private void GenerateObstacles() {
        foreach (var obstaclePosition in obstaclePositions) {
            Transform tile = GenerateTile(obstacleTilePrefab, obstaclePosition.x, obstaclePosition.y, 0, 0);
            replaceInTileMap(obstaclePosition.x, obstaclePosition.y, tile);
        }
    }

    private void GenerateBarrels() {
        foreach (var barrelPosition in barrelPositions) {
            Transform tile = GenerateTile(barrelTilePrefab, barrelPosition.x, barrelPosition.y, 0, 0);
            replaceInTileMap(barrelPosition.x, barrelPosition.y, tile);
        }
    }

    private Transform GenerateTile(Transform prefab, int x, int y, int z, float rotation) {
        Vector3 pos = new Vector3(mapSize.x / 2 - x, z, mapSize.y / 2 - y); // mapSize / 2 to center the map
        Transform tile = Instantiate(prefab, relativePosition(pos), Quaternion.Euler(0, rotation, 0)) as Transform;
        tile.parent = mapHolder;
        return tile;
    }

    /** Position functions */

    private bool isInMap(float x, float y) {
        return x >= 0 && x < mapSize.x && y >= 0 && y < mapSize.y;
    }

    private Vector3 relativePosition(Vector3 pos) {
        return pos + transform.position;
    }

    /** Map manipulation */
    private void InitTileMap() {
        tileMap = new Transform[mapSize.x][];
        for (int i = 0; i < mapSize.x; i++)
            tileMap[i] = new Transform[mapSize.y];
    }

    private void deleteInTileMap(int x, int y) {
        if (isInMap(x, y)) {
            Transform previousTile = tileMap[x][y];
            if (previousTile)
                DestroyImmediate(previousTile.gameObject); // use of DestroyImmediate to handle the editor mode.
        }
    }

    private void saveInTileMap(int x, int y, Transform tile) {
        if (isInMap(x,y))
            tileMap[x][y] = tile;
        else
            DestroyImmediate(tile.gameObject); // use of DestroyImmediate to handle the editor mode.
    }

    private void replaceInTileMap(int x, int y, Transform tile) {
        deleteInTileMap(x, y);
        saveInTileMap(x, y, tile);
    }

    private void IgReplaceInTileMap(int x, int y, Transform tile) {
        if (isInMap(x, y)) {
            Transform previousTile = tileMap[x][y];
            if (previousTile)
                Destroy(previousTile.gameObject); // // use of Destroy out of editor mode
            tileMap[x][y] = tile;
        }
        else
            Destroy(tile.gameObject); // use of Destroy out of editor mode
        
        saveInTileMap(x, y, tile);
    }
}
