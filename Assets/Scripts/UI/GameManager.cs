﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Button quitButton = null;

    [SerializeField]
    private Slider generalVolumeSlider;

    [SerializeField]
    private Text generalVolumeText;

    public float generalVolume = 100;
    public static GameManager instance;

    private void awake()
    {
        if(instance)
        {
            Debug.LogError("Another instance of the game manager " + name + " is already running.");
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        quitButton.onClick.AddListener(()=>QuitGame());
        UpdateGeneralVolume();

        DontDestroyOnLoad(gameObject);
    }

    public void UpdateGeneralVolume()
    {
        generalVolume = generalVolumeSlider.value;
        generalVolumeText.text = generalVolume.ToString("f0") + ("%");
    }

    private void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}
