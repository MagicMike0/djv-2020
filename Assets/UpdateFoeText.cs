﻿using UnityEngine;
using TMPro;

public class UpdateFoeText : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI waveText = null;

    private void FixedUpdate() {
        waveText.SetText("Enemies restants : " + MapManager.GetInstance().getCurrentWaveFoeCount());
    }
}
