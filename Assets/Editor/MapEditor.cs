﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (MapGenerator))]
public class MapEditor : Editor {

    // private bool liveRefreshInEditor = false;
    // private bool refreshed = false;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        
        MapGenerator map = target as MapGenerator;
        // if (!refreshed || liveRefreshInEditor) {
        //     map.GenerateMap();
        //     refreshed = true;
        // }

        if (GUILayout.Button("Refresh")) {
            map.GenerateMap();
        }
    }
}