﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeSlider : MonoBehaviour {

    [SerializeField]
    private Slider hpslider = null;

    [SerializeField]
    private Player player = null;

    private void FixedUpdate() {
        if (player && hpslider) {
            hpslider.value = ((float)player.hp) / ((float)player.maxHP);
        }
    }
}
